#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

const int N = 1e6;
const int L = 20;
#include "../../code/graphs/lca.cpp"

TEST(Simple, Big) {
    for (int i = 2; i < N; i++) {
        adj[i / 2].push_back(i);
        adj[i].push_back(i / 2);
    }
    init(1);
    EXPECT_EQ(lca(2, 3), 1);
    EXPECT_EQ(lca(9, 5), 2);
    EXPECT_EQ(lca(560, 561), 280);
    EXPECT_EQ(lca(900000, 3), 3);
    EXPECT_EQ(lca(1234, 4321), 4);
}
