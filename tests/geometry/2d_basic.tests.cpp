#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

using coord = long double;
const long double EPS = 1e-8;

#include "../../code/geometry/2d_basic.cpp"

void COMPARE(point a, point b) {
    EXPECT_NEAR(a.X, b.X, EPS);
    EXPECT_NEAR(a.Y, b.Y, EPS);
}

TEST(Cross, Simple) {
    point p = {2, 3}, q = {4, 5};
    EXPECT_NEAR(cross(p, q), -2, EPS);
}

TEST(Dot, Simple) {
    point p = {2, 3}, q = {3, -1};
    EXPECT_NEAR(dot(p, q), 3, EPS);
}

TEST(Rotate, Simple) {
    COMPARE(rotate90(point(1, -1)), point(1, 1));
    COMPARE(rotate(point(1, 0), PI / 4), point(1, 1) * (sqrt(2) / 2.0L));
    COMPARE(rotate(point(1, 0), 2 * PI), point(1, 0));
}

TEST(Projection, Simple) {
    COMPARE(projection(point(1, 0), point(1, 1)), point(1, 1) / 2.0L);
}

TEST(Normalized, Simple) {
    COMPARE(normalized(point(1, 1)), point(1, 1) * (sqrt(2) / 2.0L));
    COMPARE(normalized(point(0.5, 0)), point(1, 0));
}

TEST(Line, Perpendicular) {
    line l1 = line(point(1, 0), point(3, 3));
    EXPECT_NEAR(dot(perpendicular(l1).p, l1.p), 0, EPS);
}

TEST(Line, Parallel) {
    line l1 = line(point(1, 0), point(3, 3));
    line l2 = parallel(l1, point(4, 0));
    EXPECT_TRUE(contains(l2, point(4, 0)));
    EXPECT_NEAR(cross(l1.p, l2.p), 0, EPS);
}

TEST(Line, Contains) {
    line l = line(point(0, -2), point(2, 0));
    EXPECT_TRUE(contains(l, point(1, -1)));
    EXPECT_FALSE(contains(l, point(1.1, -1)));
}

TEST(Line, Intersection) {
    line l1 = line(point(0, -2), point(2, 0));
    line l2 = line(point(-1, 1), point(1, 1));
    EXPECT_TRUE(intersects(l1, l2));
    COMPARE(intersection(l1, l2), point(3, 1));
    EXPECT_FALSE(intersects(l1, parallel(l1, point(0, 0))));
}

TEST(Line, Distance) {
    line l = line(point(0, -1), point(1, 0));
    EXPECT_NEAR(distance_sq(l, point(2, 0)), 0.5, EPS);
}

TEST(Segment, Contains) {
    segment s = {point(1, 1), point(3, 3)};
    EXPECT_TRUE(contains(s, point(2, 2)));
    EXPECT_FALSE(contains(s, point(4, 4)));
}

TEST(Segment, Intersection) {
    segment s = {point(1, 1), point(3, 3)}, t = {point(1, 3), point(4, 2)}, u = {point(4, 4), point(5, 5)};
    EXPECT_TRUE(intersects(s, t));
    EXPECT_FALSE(intersects(s, u));
    EXPECT_FALSE(intersects(t, u));
}

TEST(Segment, Distance) {
    segment s = {point(1, 1), point(3, 3)};
    EXPECT_NEAR(distance_sq(s, point(1, 0)), 1, EPS);
    EXPECT_NEAR(distance_sq(s, point(3, 1)), 2, EPS);
    EXPECT_NEAR(distance_sq(s, point(2, 2)), 0, EPS);
}

TEST(Circle, Contains) {
    circle c = {point(1, 1), sqrt(5)};
    EXPECT_FALSE(contains(c, point(1, 1)));
    EXPECT_TRUE(contains(c, point(3, 2)));
    EXPECT_FALSE(contains(c, point(3.3, 1)));
}

TEST(Circle, Tangents) {
    circle c = {point(1, 1), 1};
    pair<point, point> t = tangents(c, point(2, 2));
    COMPARE(t.first, point(2, 1));
    COMPARE(t.second, point(1, 2));
    t = tangents(c, point(2, 1));
    COMPARE(t.first, point(2, 1));
    COMPARE(t.second, point(2, 1));
}

TEST(Circle, Intersection) {
    circle c1 = {point(1, 1), 2}, c2 = {point(2, 1.5), 0.5}, c3 = {point(2.5, 0), 1};
    EXPECT_FALSE(intersects(c1, c2));
    EXPECT_TRUE(intersects(c1, c3));
    EXPECT_FALSE(intersects(c2, c3));
    pair<point, point> t = intersection(c1, c3);
    EXPECT_TRUE(contains(c1, t.first));
    EXPECT_TRUE(contains(c1, t.second));
    EXPECT_TRUE(contains(c3, t.first));
    EXPECT_TRUE(contains(c3, t.second));
}

TEST(Circle, Intersection2) {
    circle c1 = {point(0, 0), 0.625}, c2 = {point(1.0000000000000004, 0), 0.3750000000000004};
    EXPECT_FALSE(intersects(c1, c2));
}
