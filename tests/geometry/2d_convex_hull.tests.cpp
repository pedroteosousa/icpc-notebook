#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

using coord = long double;
const long double EPS = 1e-8;

#include "../../code/geometry/2d_basic.cpp"
#include "../../code/geometry/2d_convex_hull.cpp"

void COMPARE(point a, point b) {
    EXPECT_NEAR(a.X, b.X, EPS);
    EXPECT_NEAR(a.Y, b.Y, EPS);
}

TEST(ConvexHull, Simple) {
    vector<point> p({{-2, 2}, {-4, 2}, {-5, 3}, {-3, 4}, {-4, 5}, {-1, 4}, {0, 3}, {-1, 1}, {-1, 6}, {-2, 5}, {0, 4}, {-5, 2}});
    vector<point> h({p[11], p[2], p[4], p[8], p[10], p[6], p[7]});
    p = convex_hull(p);
    EXPECT_EQ((int)p.size(), 7);
    for (int i = 0; i < (int)h.size(); i++)
        COMPARE(p[i], h[i]);
}
