#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

typedef long long ll;

#include "../../code/math/mul_mod.cpp"
#include "../../code/math/rabin_miller.cpp"

TEST(RabinMillerTests, SmallPrimes) {
	for (int p : {2,3,5,7,11,13,19,292911197,1000000007,1000000009})
		EXPECT_EQ(is_probably_prime(p), true);
}

TEST(RabinMillerTests, SmallComposite) {
	for (int p : {1,4,10,21,278,1000000011})
		EXPECT_EQ(is_probably_prime(p), false);
}

TEST(RabinMillerTests, MediumPrime) {
	EXPECT_EQ(is_probably_prime(1000000009), true);
}

TEST(RabinMillerTests, MediumComposite) {
	EXPECT_EQ(is_probably_prime(100362357), false);
}

TEST(RabinMillerTests, LargePrime) {
	EXPECT_EQ(is_probably_prime(25641026051282053ll), true);
}

TEST(RabinMillerTests, LargeComposite) {
	EXPECT_EQ(is_probably_prime(1789698719427691139ll), false);
}
