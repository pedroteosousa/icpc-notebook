#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

const int N = 1e6;
#include "../../code/math/phi.cpp"

TEST(Phi, Sieve) {
    phi_sieve();
    EXPECT_EQ(phi[0], 0);
    EXPECT_EQ(phi[1], 1);
    EXPECT_EQ(phi[2], 1);
    EXPECT_EQ(phi[278], 138);
    EXPECT_EQ(phi[123456], 41088);
}

TEST(Phi, Large) {
    EXPECT_EQ(phi_ll(1000000007ll), 1000000006ll);
    EXPECT_EQ(phi_ll(734917329487ll), 639058547160ll);
}
