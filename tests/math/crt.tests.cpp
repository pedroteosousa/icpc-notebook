#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

#include "../../code/math/crt.cpp"

TEST(CRT, Coprimes) {
    vector<long long> a({2, 3});
    vector<long long> m({4, 5});
    pair<long long, long long> r = crt(a, m);
    EXPECT_EQ(r.first, 18);
    EXPECT_EQ(r.second, 20);
}

TEST(CRT, Big) {
    vector<long long> a({1000000006, 1000000008});
    vector<long long> m({1000000007, 1000000009});
    pair<long long, long long> r = crt(a, m);
    EXPECT_EQ(r.first, 1000000016000000062ll);
    EXPECT_EQ(r.second, 1000000016000000063ll);
}
