#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

#include "../../code/math/mul_mod.cpp"
#include "../../code/math/pollard_rho.cpp"

TEST(PollardRhoTests, LargeNumber) {
    long long c = 1000000016000000063ll;
    long long factor = pollard_rho(c);
    EXPECT_EQ(min(factor, c / factor), 1000000007ll);
}

TEST(PollardRhoTests, Prime) {
    long long c = 1000000009;
    long long factor = pollard_rho(c);
    EXPECT_EQ(factor, 0);
}

TEST(PollardRhoTests, Even) {
    long long c = 2 * 1000000007;;
    long long factor = pollard_rho(c);
    EXPECT_EQ(min(factor, c / factor), 2);
}
