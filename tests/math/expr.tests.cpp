#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

const long long MOD = 1e9 + 7;
#include "../../code/math/expr.cpp"

TEST(Fastxp, Simple) {
    EXPECT_EQ(expr(2, 1000000005), 500000004);
    EXPECT_EQ(expr(2, 1234123412341234), 660529399);
}
