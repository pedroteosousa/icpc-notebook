#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

typedef long long ll;

const int N = 1e4;
const ll mod = 1e9+7;
#include "../../code/math/matrix.cpp"

struct num {
	ll v;
	num(ll _v = 0): v(_v) {}
	ll operator= (ll _v) {
		return v = _v;
	}
	num operator* (num o) {
		return num((v * o.v) % mod);
	}
	num operator+ (num o) {
		return num((v + o.v) % mod);
	}
};

TEST(MatrixTests, LargeMatrixExponentiation) {
	matrix<num, 100> m;
	for (int i=0;i<100;i++)
		for (int j=0;j<100;j++)
			m.m[i][j] = 1e9;

	m = m * m;
	for (int i=0;i<100;i++)
		for (int j=0;j<100;j++)
			EXPECT_EQ(m.m[i][j].v, 4900);
}

TEST(MatrixTests, Fibonacci) {
	matrix<num, 2> fib;
	fib.m[0][0] = 1;
	fib.m[0][1] = 1;
	fib.m[1][0] = 1;
	fib = fib.expr(98);
	EXPECT_EQ(fib.m[0][0].v, 94208912);
}

TEST(MatrixTests, ExponentiationWithDouble) {
	double base[3][3] = {
		{0, -1, 10},
		{1, 0, 0},
		{0, 0, 1}
	};
	matrix<double, 3> rot_and_trans(base);
	rot_and_trans = rot_and_trans.expr(3);
	EXPECT_EQ(rot_and_trans.m[0][2], 0);
	EXPECT_EQ(rot_and_trans.m[1][2], 10);
}
