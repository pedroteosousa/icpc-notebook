#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

#include "../../code/strings/suffix_array.cpp"

TEST(CheckOrdered, Small) {
    string s("abacabadabacaba");
    int n = s.size();
    suffix_array sa(s);
    for (int i = 1; i < n; i++) {
        EXPECT_TRUE(s.substr(sa.p[i - 1], n - sa.p[i - 1]) < s.substr(sa.p[i], n - sa.p[i]));
    }
}

TEST(CountDistinctSubstrings, Small) {
    string s("ababa");
    int n = s.size();
    suffix_array sa(s);
    int tot = 0;
    for (int i = 0; i < n; i++)
        tot += n - sa.p[i] - sa.lcp[i];
    EXPECT_EQ(tot, 9);
}
