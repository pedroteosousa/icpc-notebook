#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

#include "../../code/strings/aho_corasick.cpp"

TEST(MultipleStringMatch, Small) {
    vector<string> patterns({"abbb", "b", "bbb", "bc", "c"});
    vector<int> expected_freq({1, 6, 2, 1, 1});
    vector<int> freq(patterns.size(), 0);
    aho_corasick ac;
    for (int i = 0; i < int(patterns.size()); i++)
        ac.insert(patterns[i], i);
    ac.build_links();
    string s("abbcabbbb");
    int current = 0;
    for (int i = 0; i < int(s.size()); i++) {
        current = ac.next(current, s[i]);
        int link = current;
        while (link != -1) {
            if (ac.nodes[link].end)
                freq[ac.nodes[link].id]++;
            link = ac.nodes[link].endlink;
        }
    }
    for (int i = 0; i < int(expected_freq.size()); i++)
        EXPECT_EQ(expected_freq[i], freq[i]);
}
