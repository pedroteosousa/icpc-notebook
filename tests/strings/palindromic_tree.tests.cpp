#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

#include "../../code/strings/palindromic_tree.cpp"

TEST(CountDistinctPalindromes, Small) {
    palindromic_tree p;
    string s("abacabadabacaba");
    p.insert(s);
    EXPECT_EQ(int(p.nodes.size()) - 2, 15);
}

TEST(CountDistinctPalindromes, MultipleStrings) {
    string s1("abacabadabacaba");
    string s2("zabazoozabaz");
    string s3("zabacabaz");
    palindromic_tree p;
    p.insert(s1);
    p.insert(s2);
    p.insert(s3);
    EXPECT_EQ(int(p.nodes.size()) - 2, 25);
}
