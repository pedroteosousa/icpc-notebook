#include <bits/stdc++.h>
#include "gtest/gtest.h"
using namespace std;

const int N = 1e5;
#include "../../code/data_structures/bit.cpp"

TEST(Simple, Small) {
    BIT b;
    vector<int> v({1, -5, 3, 8, 22});
    vector<int> acc;
    for (int sum = 0, i = 0; i < (int)v.size(); i++) {
        sum += v[i];
        acc.push_back(sum);
        b.update(i, v[i]);
    }
    for (int i = 0; i < (int)acc.size(); i++) {
        EXPECT_EQ(b.get_sum(i), acc[i]);
    }
}
