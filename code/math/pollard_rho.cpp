long long pollard_rho(long long n) {
    mt19937_64 rng(chrono::steady_clock::now().time_since_epoch().count());
    uniform_int_distribution<long long> distrib(2, n-1);
    long long x = distrib(rng), y = x, c = distrib(rng), factor;
    do {
        x = (mul_mod(x, x, n) + c) % n;
        y = (mul_mod(y, y, n) + c) % n;
        y = (mul_mod(y, y, n) + c) % n;
        factor = __gcd(abs(y - x), n);
    } while (factor == 1);
    if (factor == n)
        return 0;
    return factor;
}
