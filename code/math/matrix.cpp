template <class T, int N>
struct matrix {
    T m[N][N];
    matrix() {
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                m[i][j] = 0;
    }
    matrix(T _m[][N]) {
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                m[i][j] = _m[i][j];
    }
    static matrix identity() {
        matrix id;
        for (int i = 0; i < N; i++)
            id.m[i][i] = 1;
        return id;
    }
    matrix operator* (matrix &o) {
        matrix r;
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                for (int k = 0; k < N; k++)
                    r.m[i][j] = r.m[i][j] + (m[i][k] * o.m[k][j]);
        return r;
    }
    matrix expr(long long e) {
        if (!e)
            return matrix::identity();
        matrix temp = expr(e >> 1);
        temp = temp * temp;
        if (e & 1)
            temp = temp * (*this);
        return temp;
    }
};
