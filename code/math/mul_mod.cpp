// a * b % m
long long mul_mod(long long a, long long b, long long m) {
    long long x = 0, y = a%m;
    while (b) {
        if (b % 2)
            x = (x+y)%m;
        y = (2*y)%m;
        b >>= 1;
    }
    return x%m;
}

// n ^ e % m
long long exp_mod(long long n, long long e, long long m) {
    if (e == 0)
        return 1ll;
    long long temp = exp_mod(mul_mod(n, n, m), e/2, m);
    if (e & 1)
        temp = mul_mod(n, temp, m);
    return temp;
}
