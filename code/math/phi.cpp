int phi[N];
void phi_sieve() {
    for (int i = 0; i < N; i++)
        phi[i] = i;
    for (int i = 2; i < N; i++) {
        if (phi[i] == i) {
            for (int j = i; j < N; j += i) {
                phi[j] -= phi[j] / i;
            }
        }
    }
}

long long phi_ll(long long x) {
    long long result = x;
    for (long long p = 2; p * p <= x; p++) {
        if (x % p == 0) {
            while (x % p == 0)
                x /= p;
            result -= result / p;
        }
    }
    if (x > 1)
        result -= result / x;
    return result;
}
