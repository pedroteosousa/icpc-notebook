long long ext_gcd(long long a, long long b, long long &x, long long &y){
    if (!b) {
        x = 1;
        y = 0;
        return a;
    }
    long long ans = ext_gcd(b, a % b, x, y);
    long long t = x;
    x = y;
    y = t - a / b * y;
    return ans;
}

pair<long long, long long> crt(vector<long long> &a, vector<long long> &m) {
    long long af = a[0];
    long long mf = m[0];
    for (int i = 1; i < (int)a.size(); i++) {
        long long gcd = __gcd(m[i], mf);
        if (af % gcd != a[i] % gcd)
            return {-1, -1};
        long long p, q;
        ext_gcd(mf / gcd, m[i] / gcd, p, q);

        long long lcm = mf / gcd * m[i];
        auto careful = [&lcm](long long v1, long long v2, long long v3) -> long long {
            return (((v1 * v2) % lcm) * v3) % lcm;
        };
        long long x = (careful(af, m[i] / gcd, q) + careful(a[i], mf / gcd, p)) % lcm;

        af = x + (x < 0 ? lcm : 0);
        mf = lcm;
    }
    return {af, mf};
}
