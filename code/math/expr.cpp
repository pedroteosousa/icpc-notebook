long long expr(long long e, long long n) {
    if (!n) return 1ll;
    return (((n & 1) ? e : 1) * expr((e * e) % MOD, n / 2)) % MOD;
}
