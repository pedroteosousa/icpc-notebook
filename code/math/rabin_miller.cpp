// complexity: O(t*log2^3(p))
bool is_probably_prime(long long p, long long t=64) {
    if (p <= 1) return false;
    if (p <= 3) return true;
    mt19937_64 rng(chrono::steady_clock::now().time_since_epoch().count());
    uniform_int_distribution<long long> distrib(2, p - 2);
    long long r = 0, d = p - 1;
    while (d % 2 == 0) {
        r++;
        d >>= 1;
    }
    while (t--) {
        long long a = distrib(rng);
        a = exp_mod(a, d, p);
        if (a == 1 || a == p - 1) continue;
        for (int i = 0; i < r - 1; i++) {
            a = mul_mod(a, a, p);
            if (a == 1) return false;
            if (a == p - 1) break;
        }
        if (a != p - 1) return false;
    }
    return true;
}
