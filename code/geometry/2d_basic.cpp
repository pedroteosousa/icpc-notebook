//using coord = long double;
//const coord EPS = 1e-8;

namespace basic {
    #define X real()
    #define Y imag()
    const long double PI = 4 * atan(1);

    int signal(coord x) { return (x > EPS) - (x < -EPS); }
    coord sq(coord x) { return x * x; }

    using point = complex<coord>;
    coord dot(point a, point b) { return (conj(a) * b).X; }
    coord cross(point a, point b) { return (conj(a) * b).Y; }
    point rotate90(point p) { return p * point(0, 1); }
    point rotate(point p, long double ang) { return p * polar(1.0L, ang); }
    point projection(point p, point d) { return d * (dot(p, d) / norm(d)); }
    point normalized(point p) { return p / abs(p); }

    struct line {
        point p; coord c;
        line() {}
        line(point a, point b): p(rotate90(b - a)), c(dot(p, a)) {}
        line(point p, coord c): p(p), c(c) {}
    };
    line perpendicular(line l) { return {rotate90(l.p), l.c}; }
    line parallel(line l, point v) { return {l.p, dot(l.p, v)}; }
    bool contains(line l, point v) { return signal(l.c - dot(l.p, v)) == 0; }
    bool intersects(line l1, line l2) { return signal(cross(l1.p, l2.p)); }
    point intersection(line l1, line l2) {
        if (!intersects(l1, l2)) throw 1;
        point p(cross({l1.c, l1.p.Y}, {l2.c, l2.p.Y}), cross({l1.p.X, l1.c}, {l2.p.X, l2.c}));
        return p / cross(l1.p, l2.p);
    }
    coord distance_sq(line l, point v) { return sq(l.c - dot(l.p, v)) / norm(l.p); }

    struct segment { point a, b; };
    bool contains(segment s, point p) {
        return signal(cross(p - s.a, s.b - s.a)) == 0 && signal(dot(p - s.a, p - s.b)) <= 0;
    }
    bool intersects(segment s1, segment s2) {
        if (contains(s1, s2.a) || contains(s1, s2.b) || contains(s2, s1.a) || contains(s2, s1.b))
            return true;
        auto side = [](point p, segment s) { return signal(cross(s.b - s.a, p - s.a)); };
        return side(s2.a, s1) * side(s2.b, s1) == -1 && side(s1.a, s2) * side(s1.b, s2) == -1;
    }
    coord distance_sq(segment s, point p) {
        if (signal(dot(s.b - s.a, p - s.a)) * signal(dot(s.a - s.b, p - s.b)) >= 0)
            return distance_sq(line(s.a, s.b), p);
        return min(norm(p - s.a), norm(p - s.b));
    }

    struct circle { point c; coord r; };
    bool contains(circle c, point p) { return signal(norm(p - c.c) - sq(c.r)) == 0; } // border
    pair<point, point> tangents(circle c, point p) {
        point d = p - c.c;
        long double cs = c.r / abs(d);
        if (cs > 1.0) throw 1;
        d *= cs;
        long double ang = acos(cs);
        return {c.c + rotate(d, -ang), c.c + rotate(d, ang)};
    }
    bool intersects(circle c1, circle c2) {
        coord d = norm(c1.c - c2.c);
        // return sq(c1.r - c2.r) <= d && d <= sq(c1.r + c2.r); // int precision
        long double cs = (sq(c2.r) - sq(c1.r) - d) / (-2.0L * sqrt(d) * c1.r);
        return abs(cs) <= 1;
    }
    pair<point, point> intersection(circle c1, circle c2) {
        if (!intersects(c1, c2)) throw 1;
        point d = c2.c - c1.c;
        long double ang = acos((sq(c2.r) - sq(c1.r) - norm(d)) / (-2.0L * abs(d) * c1.r));
        d *= c1.r / abs(d);
        return {c1.c + rotate(d, -ang), c1.c + rotate(d, ang)};
    }
};
using namespace basic;
