vector<point> convex_hull(vector<point> p) {
    if (p.size() == 1) return p;
    auto cmp = [](point a, point b) { return make_pair(a.X, a.Y) < make_pair(b.X, b.Y); };
    sort(p.begin(), p.end(), cmp);
    point l = p[0], r = p.back();
    vector<point> up({l}), down({l});
    auto side = [](point a, point b, point c) { return signal(cross(b - a, c - a)); };
    for (int i = 1; i < (int)p.size(); i++) {
        // to include collinear points, change > to >=, >= to >, < to <= and <= to <
        if (i == (int)p.size() - 1 || side(l, r, p[i]) > 0) {
            while (up.size() >= 2 && side(up[(int)up.size() - 2], up.back(), p[i]) >= 0)
                up.pop_back();
            up.push_back(p[i]);
        }
        if (i == (int)p.size() - 1 || side(l, r, p[i]) < 0) {
            while (down.size() >= 2 && side(down[(int)down.size() - 2], down.back(), p[i]) <= 0)
                down.pop_back();
            down.push_back(p[i]);
        }
    }
    vector<point> h;
    for (int i = 0; i < (int)up.size(); i++)
        h.push_back(up[i]);
    for (int i = (int)down.size() - 2; i > 0; i--)
        h.push_back(down[i]);
    return h;
}
