vector<int> adj[N];

stack<int> ts;
int tme = 0, ncomp = 0, low[N], seen[N];
int comp[N]; // nodes in the same scc have the same color

// time complexity: O(V+E)
int scc_dfs(int u) {
    seen[u] = low[u] = ++tme;
    ts.push(u);
    for (auto v : adj[u]) {
        if (seen[v] == 0)
            scc_dfs(v);
        low[u] = min(low[u], low[v]);
    }
    if (low[u] == seen[u]) {
        int node;
        do {
            node = ts.top(); ts.pop();
            comp[node] = ncomp;
            low[node] = N;
        } while (u != node && ts.size());
        ncomp++;
    }
    return low[u];
}
