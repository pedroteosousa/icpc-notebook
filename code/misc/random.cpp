// mt19937_64 for long long
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

// random_shuffle
shuffle(v.begin(), v.end(), rng);

// random number in [a, b]
uniform_int_distribution<int> dist(a, b);
dist(rng);
