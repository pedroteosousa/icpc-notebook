// 0 indexed
struct BIT {
    int b[N];
    BIT() { memset(b, 0, sizeof(b)); }
    void update(int p, int val) {
        for(p++; p < N; p += p & -p)
            b[p] += val;
    }
    int get_sum(int p) {
        int sum = 0;
        for(p++; p != 0; p -= p & -p)
            sum += b[p];
        return sum;
    }
};
