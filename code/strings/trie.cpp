struct trie {
    struct node {
        map<char, int> to;
        int freq = 0, end = 0;
    } t[N];
    int sz = 1;

    void insert(char *s, int root = 0) {
        t[root].freq++;
        if (*s == 0) {
            t[root].end++;
            return;
        }
        if (t[root].to.find(*s) == t[root].to.end())
            t[root].to[*s] = sz++;
        insert(s+1, t[root].to[*s]);
    }
    bool remove(char *s, int root = 0) {
        if (*s == 0 && t[root].end) {
            t[root].freq--;
            t[root].end--;
            return true;
        }
        if (t[root].to.find(*s) == t[root].to.end())
            return false;
        bool removed = remove(s+1, t[root].to[*s]);
        if (removed)
            t[root].freq--;
        return removed;
    }
    int find(char *s, int root = 0) {
        if (*s == 0)
            return t[root].end;
        if (t[root].to.find(*s) == t[root].to.end())
            return 0;
        return find(s+1, t[root].to[*s]);
    }
    int count(char *p, int root = 0) {
        if (*p == 0)
            return t[root].freq;
        if (t[root].to.find(*p) == t[root].to.end())
            return 0;
        return count(p+1, t[root].to[*p]);
    }
};
