struct aho_corasick {
    struct node {
        map<char, int> to;
        int link, endlink, id;
        bool end;
        bool has(char c) { return to.find(c) != to.end(); }
        node() { end = false; }
    };
    vector<node> nodes;
    aho_corasick() { nodes.emplace_back(); }
    void insert(string& s, int id) {
        int current = 0;
        for (auto c: s) {
            if (!nodes[current].has(c)) {
                nodes[current].to[c] = nodes.size();
                nodes.emplace_back();
            }
            current = nodes[current].to[c];
        }
        nodes[current].end = true;
        nodes[current].id = id;
    }
    void build_links() {
        queue<int> q;
        nodes[0].link = nodes[0].endlink = -1;
        q.push(0);
        while (q.size()) {
            int u = q.front();
            q.pop();
            for (auto e: nodes[u].to) {
                int& link = nodes[e.second].link;
                link = nodes[u].link;
                while (link != -1 && !nodes[link].has(e.first))
                    link = nodes[link].link;
                link = (link == -1) ? 0 : nodes[link].to[e.first];
                nodes[e.second].endlink = nodes[link].end ? link : nodes[link].endlink;
                q.push(e.second);
            }
        }
    }
    int next(int current, char c) {
        // TODO: this only needs to be done once and then can be memoized
        while (current != -1 && !nodes[current].has(c))
            current = nodes[current].link;
        if (current == -1)
            return 0;
        return nodes[current].to[c];
    }
};
