struct suffix_array {
    const char* s;
    int n;
    vector<int> p, r, lcp;
    suffix_array(string& str) : s(str.c_str()), n(str.size()), p(n), r(n), lcp(n) {
        vector<int> aux(n);
        for (int i = 0; i < n; i++)
            r[p[i] = i] = s[i];
        for (int d = 1; ; d *= 2) {
            csort(d, aux); csort(0, aux);
            auto cmp = [&d, this](int i, int j) {
                return make_pair(rank(i), rank(i + d)) < make_pair(rank(j), rank(j + d));
            };
            aux[p[0]] = 0;
            for (int i = 1, j = 0; i < n; i++, j++)
                aux[p[i]] = aux[p[j]] + cmp(p[j], p[i]);
            copy(aux.begin(), aux.end(), r.begin());
            if (r[p[n - 1]] == n - 1) break;
        }
        build_lcp();
    }
    void build_lcp() {
        for (int i = 0, h = 0; i < n; i++, h = max(0, h - 1)) {
            if (r[i] != n - 1) {
                int j = p[r[i] + 1];
                while (i + h < n && j + h < n && s[i + h] == s[j + h]) h++;
            }
            lcp[r[i]] = h;
        }
    }
private:
    int rank(int p) { return p < n ? r[p] : -1; };
    void csort(int d, vector<int>& aux) {
        vector<int> freq(3 + max<int>(numeric_limits<char>::max(), n - 1), 0);
        for (int i: p) freq[rank(i + d) + 2]++;
        partial_sum(freq.begin(), freq.end(), freq.begin());
        for (int i: p) aux[freq[rank(i + d) + 1]++] = i;
        copy(aux.begin(), aux.end(), p.begin());
    };
};
