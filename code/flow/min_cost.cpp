// using num = long long;
// const num INF = 1e12;

struct min_cost {
    struct edge {
        int from, to;
        num cp, fl, cs;
    };
    vector<edge> edges;
    vector<int> adj[N];

    void add_edge(int i, int j, num cp, num cs) {
        edges.push_back({i, j, cp, 0, cs}); adj[i].push_back(edges.size() - 1);
        edges.push_back({j, i, 0, 0, -cs}); adj[j].push_back(edges.size() - 1);
    }

    int seen[N], pai[N];
    num dist[N], cost, flow;
    int turn;
    num spfa(int s, int t) {
        turn++;
        queue<int> q; q.push(s);
        for (int i = 0; i < N; i++) dist[i] = INF;
        dist[s] = 0;
        seen[s] = turn;
        while (q.size()) {
            int u = q.front(); q.pop();
            seen[u] = 0;
            for (auto e : adj[u]) {
                int v = edges[e].to;
                if (edges[e].cp > edges[e].fl && dist[u] + edges[e].cs < dist[v]) {
                    dist[v] = dist[u] + edges[e].cs;
                    pai[v] = e ^ 1;
                    if (seen[v] < turn) {
                        seen[v] = turn;
                        q.push(v);
                    }
                }
            }
        }
        if (dist[t] == INF) return 0;
        num nfl = INF;
        for (int u = t; u != s; u = edges[pai[u]].to)
            nfl = min(nfl, edges[pai[u] ^ 1].cp - edges[pai[u] ^ 1].fl);
        cost += dist[t] * nfl;
        for (int u = t; u != s; u = edges[pai[u]].to) {
            edges[pai[u]].fl -= nfl;
            edges[pai[u] ^ 1].fl += nfl;
        }
        return nfl;
    }

    void mncost(int s, int t) {
        cost = flow = 0;
        while (num fl = spfa(s, t))
            flow += fl;
    }
};
